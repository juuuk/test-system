Rails.application.routes.draw do
  devise_for :users
  get 'lessons/index'
  get 'lessons/view'
  get 'lessons/start/:id' => 'lessons#view', as: 'start_test'
  post 'lessons/start/:id' => 'lessons#answer', as: 'answer_for_test'
  get 'lessons/results/:id' => 'lessons#results', as: 'results_test'

  resources :answers
  resources :questions
  resources :tests do
    post 'check_all_user_at_group', on: :member
    post 'check_user_for', on: :member

  end
  resources :users
  resources :groups
  get 'admin/index'
  root 'home#index'
  get 'lessons/index', as: 'user_root'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
