class CreateTestsForUsers < ActiveRecord::Migration
  def change
    create_table :tests_for_users do |t|
      t.integer :user_id
      t.integer :test_id

      t.timestamps null: false
    end
  end
end
