class CreateTestUserLogs < ActiveRecord::Migration
  def change
    create_table :test_user_logs do |t|
      t.integer :question_id
      t.time :question_start_time
      t.time :all_time
      t.integer :user_id
      t.integer :point

      t.timestamps null: false
    end
  end
end
