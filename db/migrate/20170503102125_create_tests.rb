class CreateTests < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.string :text
      t.integer :credit
      t.integer :itime
      t.timestamps null: false
    end
  end
end
