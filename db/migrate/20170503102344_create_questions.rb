class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :test_id
      t.integer :flag
      t.string :text
      t.timestamps null: false
    end
  end
end
