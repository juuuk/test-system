class User < ActiveRecord::Base
  belongs_to :group
  has_many :tests_for_users
  has_many :test_user_logs

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


end
