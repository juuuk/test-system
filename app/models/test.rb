class Test < ActiveRecord::Base
  has_many :questions, dependent: :delete_all

  validates :text,  presence: true
end
