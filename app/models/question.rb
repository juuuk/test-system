class Question < ActiveRecord::Base
  default_scope { order('created_at DESC') }
  belongs_to :test
  has_many :answers


end
