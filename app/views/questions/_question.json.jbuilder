json.extract! question, :id, :test_id, :flag, :created_at, :updated_at
json.url question_url(question, format: :json)
