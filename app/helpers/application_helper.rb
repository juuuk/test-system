module ApplicationHelper
  def list_group
    Group.all.collect { |p| [ p.name, p.id ] }
  end
  def check_user(user_id, test_id)
    check = TestsForUser.find_by(user_id: user_id, test_id: test_id)
    'success' unless check.nil?
  end
end
