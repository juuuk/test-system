class TestsController < AdminController
  before_action :set_test, only: [:show, :edit, :update, :destroy]

  # GET /tests
  # GET /tests.json
  def index
    @tests = Test.all
  end

  # GET /tests/1
  # GET /tests/1.json
  def show
    @groups = Group.all
  end

  def check_user_for

    check = TestsForUser.find_by(user_id: user_params[:user_id], test_id:user_params[:id])
    p check
    if check.nil?
      TestsForUser.create(user_id: user_params[:user_id], test_id:user_params[:id])
      render js: "$('#u#{user_params[:user_id]}').addClass('success'); $('#b#{user_params[:user_id]}').text('Убрать');"
    else
      check.destroy
      render js: "$('#u#{user_params[:user_id]}').removeClass('success'); $('#b#{user_params[:user_id]}').text('Выбрать');"
    end

  end

  def check_all_user_at_group
    # сделал чтобы связи были только по юзерам, но можно и по группам отдельно через полиморфные связи
    users = User.where(group_id:user_params[:group_id])
    if users.count >= TestsForUser.where(user_id: users, test_id:user_params[:id]).count

      users.each do |item|
        TestsForUser.create(user_id: item.id, test_id:user_params[:id])
      end
      render js: "$('.g#{user_params[:group_id]}').addClass('success'); $('#ba#{user_params[:group_id]}').text('Убрать всех'); $('.bb#{user_params[:group_id]}').text('Убрать');"

    else
      users.each do |item|
        TestsForUser.find_by(user_id: item.id, test_id:user_params[:id]).destroy
      end
      render js: "$('.g#{user_params[:group_id]}').removeClass('success'); $('#ba#{user_params[:group_id]}').text('Выбрать всех'); $('.bb#{user_params[:group_id]}').text('Выбрать');"
    end

  end

  # GET /tests/new
  def new
    @test = Test.new
  end

  # GET /tests/1/edit
  def edit
  end

  # POST /tests
  # POST /tests.json
  def create
    @test = Test.new(test_params)

    respond_to do |format|
      if @test.save
        format.html { redirect_to @test, notice: 'Test was successfully created.' }
        format.json { render :show, status: :created, location: @test }
      else
        format.html { render :new }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tests/1
  # PATCH/PUT /tests/1.json
  def update
    respond_to do |format|
      if @test.update(test_params)
        format.html { redirect_to @test, notice: 'Test was successfully updated.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tests/1
  # DELETE /tests/1.json
  def destroy
    @test.destroy
    respond_to do |format|
      format.html { redirect_to tests_url, notice: 'Test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test
      @test = Test.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_params
      params.require(:test).permit(:text, :credit)
    end
  def user_params
    params.permit(:user_id, :test_id, :id, :group_id)
  end
end
