class LessonsController < ApplicationController
  before_action :authenticate_user!
  def index
    @tests = current_user.tests_for_users.all
  end

  def answer
    test = current_user.tests_for_users.find_by(test_id: params[:id]).test
    count = current_user.test_user_logs.where(question_id: test.questions).count

    right = test.questions[count].answers.find_by(flag: 1)

    current_user.test_user_logs.create(question_id: test.questions[count].id, point: answer_params[:id].to_i)

    if right.id == answer_params[:id].to_i
      render js: "$('.right').text('Вы правы!').css('color','green'); $('.form').text(''); $('.next').show(); "

    else
      render js: "$('.right').text('Вы не правы! Правильный ответ - #{right.text}').css('color','red'); $('.form').text(''); $('.next').show()"
    end
  end

  def view
   #чтобы в урл не светить ид вопроса сделал остслеживание текущего вопроса в логе прохождения теста, урл в тесте всегда будет одинаковым
    @test = current_user.tests_for_users.find_by(test_id: params[:id]).test

    count = current_user.test_user_logs.where(question_id: @test.questions).count

    if count == 0
      @question = @test.questions.first
    else
      @question = @test.questions[count]
    end
    redirect_to results_test_path(params[:id]) if count == current_user.tests_for_users.find_by(test_id: params[:id]).test.questions.all.count
  end

  def results
    @test = current_user.tests_for_users.find_by(test_id: params[:id]).test

    log = current_user.test_user_logs.where(question_id: @test.questions)
    r = 0
    log.each do |item|
      if item.point != item.question.answers.find_by(flag: 1).id
      r += 1
      end
    end
    if r > @test.credit.to_i
      @res = 'Тест не пройден'
    else
      @res = 'Поздравляем Вы прошли тест'
    end

  end

  private
  def answer_params
    params.require(:answer).permit(:id)
  end
end
